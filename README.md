# gwcloud_db_helm

## Assumptions
- Helm3 is [installed](https://helm.sh/docs/intro/install/)
- Relevant docker images and container image registry exists
- Relevant vault access and secrets exists
- Relevant storage access and storage solutions exist

## Repo Initialisation
- Initialise helm repo by executing `helm create gwcloud_db`
- Move helm output by `mv gwcloud_db/* . && mv gwcloud_db/.helmignore .`
- Cleanup `rmdir gwcloud_db`

## Chart values
Dynamic variables declared and initialised in [values.yaml](./values.yaml)
| Variable | Default | Comment |
| --- | --- | --- |
| `config_map.default_auth` | ```[mysqld] default_authentication_plugin=mysql_native_password``` | Custom mysqld configuration |
| `deployment.name` | mysql | Name value of k8s deployment resource |
| `deployment.db_config.method` | k8s_secrets | Method for populating deployment env vars.\n Acceptable values are ['k8s_secrets', 'hashicorp_vault'] |
| `image.repository` | mysql | Container image repository |
| `image.tag` | "" | Main chart application `gwcloud_db` image tag controlled through `appVersion` in [Chart.yaml](./Chart.yaml) |
| `vaultAnnotations.role` | db | Role configured at the vault server `vault.gwdc.org.au` |
| `vaultAnnotations.secrets` | [] | list of vault kv engines used as a reference for populating container env variables |

## Architecture
TBA

## Prerequisites
```bash
# Vault dependencies
#   Vault cli required
#   cli must have network access to vault
vault login -address=$VAULT_HTTPS_FQDN -method=github -token=$ACCESS_TOKEN

# Add vault policy
tee db.policy.hcl <<EOF
# Read db deployment config
path "kv/db/+/deployment"
{
  capabilities = ["list", "read"]
}
EOF
vault policy write db-admin db.policy.hcl

# Sanity Check
vault policy read db-admin

# Add vault kubernetes role
vault write auth/kubernetes/role/db \
  bound_service_account_names=db \
  bound_service_account_namespaces=db \
  policies=default,db-admin \
  ttl=1h

# Sanity Check
vault read auth/kubernetes/role/db
```

## Chart Development
```bash
# Code Linting
helm lint

# Display default helm values
helm show values .

# Render template files with values.yaml as payload
helm template .

# Packaging
#   Chart packaged as $(Chart.name)-$(Chart.version).tgz
rm -rf *.tgz
helm package $(PWD)

# Nexus upload
curl -u "$(NEXUS_ID):$(NEXUS_PASSWORD)" https://nexus.gwdc.org.au/repository/helm/ --upload-file `ls *.tgz` -v
```
## Support
TBA

## ToDo
- [x] Integrate Vault secrets.
- [x] Proof of concept deployment.
- [x] CI for testing the helm chart.
- [ ] Logic for db data level backup and restoration.
- [ ] CD for deploying packaged charts to `https://nexus.gwdc.org.au/#browse/browse:helm`
- [ ] CD Runtime test to dev k8s cluster.